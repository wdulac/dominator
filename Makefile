EXEC = dominator
SOURCES = functions.cpp domino.cpp main.cpp
CC = g++
CXXFLAGS = -Wall
LIBS = -lgsl -lgslcblas -lm

DEPEND_FILE = .depend
OBJS = $(SOURCES:%.cpp=%.o)

all: $(DEPEND_FILE) $(EXEC)

$(EXEC): $(OBJS)
	$(CC) $(OBJS) -o $(EXEC) $(LIBS)

clean:
	$(RM) *~ *.o $(DEPEND_FILE) \#*

cleanall: clean
	$(RM) $(EXEC) output/*.dat figures/*.eps

$(DEPEND_FILE): $(SOURCES)
	$(CC) -MM $(SOURCES) > $(DEPEND_FILE)

-include $(DEPEND_FILE)
