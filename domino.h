/* Dominator : Falling dominos numerical simulation
*
* Copyright (C) 2018
*
*            Dulac William    Gillet Amaury
*            Sarica Thibaud   Kraiem Samy
*
* dominator is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/* This file the class declaration for a domino.
 * It also loads header files and declares prototypes.
*/

#include "settings.h"
#include <assert.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <string>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_const_mksa.h>


class domino{

        // GSL related attributes. Without having these first three as class attributes, we wouldn't be able to delete them in the destructor.
        gsl_odeiv2_driver * gsl_driver;
        gsl_odeiv2_system * gsl_sys;
        struct func_params* gsl_params;
        double y[2]; // This is used to set initial conditions. It incidently holds the last
                     // computed solutions for our falling equation, but it's more readable
                     // to use the tilt and angular_speed public attributes. Also it's safer
                     // to not handle this array directly.

        // Domino's private attributes
        double begin_t; // Initial time of activity of a given domino
        double current_time; // This is the 'current time' of the system. It holds the last time used in the last computed angle.
        double initial_angular_speed; // It holds the initial value for y[1] for the given domino. It changes at each domino iteration.

        void init(); // Contains commond constructor code for overloading.
        void initSolver(); // Initiates the GSL driver
        void evalCollisionAngle(); // Evaluates the angle at which a domino collides with the next one. Called only once in constructor.
        void evalCollisionTime(); // Evaluates the time at which the collision happens
        void resetSolver(); // Resets the solver for the current domino
        void resetDominoChain(); // Resets the solver to the initial state of the domino chain.

        // Finding this function's root provides the collision_time. I can't think of a way of using GSL for this
        // as it won't let me declare a function pointer when the function is a class member.
        double evalTimeFunc(double);

    public:

        int N_domino; // Number of dominos in our chain.
        int index; // ID number for each domino of the chain. Should be changed only by the iterate() methods.
        double tilt; // Stores the tilt of the domino at a given time. Copied from y[0]
        double angle_error; // Stores the estimated error on the computed tilt
        double angular_speed; // Stores the domino's angular speed at a given time. Copied from y[1]
        double angular_speed_error; // Stores the domino's angular speed error at a given time.
        double elapsed_time; //Stores the cumulated falling time for each domino.
        double collision_time; // Time at whitch the domino collides as regard to the current domino.
        double collision_angle; // Collision angle of the domino
        double collision_angle_error; // Stores the estimated error on the collision angle
        double collision_time_error; // Error on the collision time.

        void evalTilt(double); // Uses GSL's solver to evaluate the domino's tilt at a given time
        void solveTiltOverTime(); // Evaluates tilt over a time range between begin_t and collision_time for the current domino and puts it in an output file
        void solveWaveSpeed(); // Evaluate propagation wave speed for each domino and stores the results in a output file.
        void iterate();// Iterates to the next domino


        domino();
        domino(int);
        ~domino();

};
