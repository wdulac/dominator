/* Dominator : Falling dominos numerical simulation
*
* Copyright (C) 2018
*
*            Dulac William    Gillet Amaury
*            Sarica Thibaud   Kraiem Samy
*
* dominator is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/* This file holds the system's constant parameters.
 * All values are named CST_XXX where XXX is the name
 * of the parameter.
*/

#ifndef settings
#define settings

// The following parameters are used by the different solvers that take place in this simulation.
// It is not necessary to modify them.

#define CST_ABS_PRECISION       1e-10 // This is the absolute precision required for the GSL solver

#define CST_REL_PRECISION       1e-10 // This is the relative precision required for the GSL solver

#define CST_SCALE_Y             1. // GSL Scaling factor for the system state y(t)

#define CST_SCALE_DY            0. // GSL Scaling factor for the system state dy(t)/dt

#define CST_INIT_STEP_SIZE      1e-3 // GSL Initial step size

// The following parameters apply to the dominos themself. It is not guaranteed the values you put in
// will work in the simulation (e.g sub-zero values).

#define CST_N_DOMINOS           100 // Default number of dominos in our system

#define CST_DIST                22.2e-3 // [m] This is the distance separating two dominos. Experiment's value

// Several mass values are provided as examples.

#define CST_MASS                3.5e-3 // [kg]. Default mass value.

//#define CST_MASS                1.75e-3 // [kg] Domino's mass. Experiment's value

//#define CST_MASS                2.24e-3 // Chalk

//#define CST_MASS                41.1e-3 // Osmium

//#define CST_MASS                34.6e-3 // Gold

//#define CST_MASS                14.1e-3 // Iron

//#define CST_MASS                4.85e-3 // Aluminium

//#define CST_MASS                6.3e-3 // Diamond

//#define CST_MASS                21.5e-3 // Palladium

//#define CST_MASS                29.8e-3 // Tantal

//#define CST_MASS                71.8e-3 // Intermediary mass

//#define CST_MASS                270e-3 // Sun

//#define CST_MASS                1.5e-3 // potassium

//#define CST_MASS                0.43e-3 // Cork

//#define CST_MASS                10e-3

#define CST_HEIGHT              3.595e-2 // [m] This is the height of our dominos. Experiment's value

#define CST_RESTING_LENGTH      0. // [m] Resting length of the spring

#define CST_K                   1200. // [N.m^-1] Spring's stiffness

#define CST_J                   (CST_MASS*CST_HEIGHT*CST_HEIGHT)/3 // [kg.m^2] Domino's inertial momemtum

#define CST_VISCOUS_FORCE       1e-5 // [N.m.rad^-1.s] Viscous force for the air

#define CST_INITIAL_TILT        0. // [rad] Initial angle of the dominos.

#define CST_INITIAL_SPEED       0.483 // [rad.s^-1] Initial angular speed. Experiment's value

#endif
