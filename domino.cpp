/* Dominator : Falling dominos numerical simulation
*
* Copyright (C) 2018
*
*            Dulac William    Gillet Amaury
*            Sarica Thibaud   Kraiem Samy
*
* dominator is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// code file for the domino header file

#include "domino.h"
#include "functions.h"

using namespace std;


domino::domino(){
   N_domino = CST_N_DOMINOS;
   init();
}


domino::domino(int N){
    N_domino = N;
    init();
}


domino::~domino(){
    gsl_odeiv2_driver_free(gsl_driver);
    delete gsl_params;
    delete gsl_sys;
}


void domino::init(){

    index                   =   0;
    begin_t                 =   0.;
    current_time            =   begin_t;
    elapsed_time            =   0.;
    tilt                    =   CST_INITIAL_TILT;
    initial_angular_speed   =   CST_INITIAL_SPEED;
    angular_speed           =   initial_angular_speed;
    initSolver();
    evalCollisionAngle();
    evalCollisionTime();
}


void domino::initSolver(){

    // The 'new' operator is used so that the function's parameters
    // as well as the gsl_odeiv2_system structure continue to live on
    // after the initSolver() method ends and therefore clears the
    // execution stack. Without it, program returns a segfault.
    gsl_params                      =   new struct func_params;
    gsl_sys                         =   new gsl_odeiv2_system;

    gsl_params->J                   =   CST_J;
    gsl_params->gamma               =   CST_VISCOUS_FORCE;
    gsl_params->m                   =   CST_MASS;
    gsl_params->g                   =   9.819721067; // Using villeurbanne's gravity accel instead of GSL_CONST_MKSA_GRAV_ACCEL
    gsl_params->h                   =   CST_HEIGHT;

    gsl_sys->function               =   &func; // Defined in functions.cpp
    gsl_sys->jacobian               =   NULL; // Runge-Kutta type solvers don't require a jacobian matrix.
    gsl_sys->dimension              =   2; // One 2nd order ODE expressed as a 2-dimension 1st order ODEs system
    gsl_sys->params                 =   gsl_params;

    y[0] = CST_INITIAL_TILT;
    y[1] = CST_INITIAL_SPEED;

    // Using Runge-Kutta-Fehlberg (4, 5) method with initial step size of CST_INIT_STEP_SIZE, absolute and relative error respectively
    // equals to CST_ABS_PRECISION and CST_REL_PRECISION and scaling factors CST_SCALE_Y and CST_SCALE_DY (from settings.h)
    gsl_driver = gsl_odeiv2_driver_alloc_standard_new(gsl_sys, gsl_odeiv2_step_rkf45, CST_INIT_STEP_SIZE, CST_ABS_PRECISION, CST_REL_PRECISION, CST_SCALE_Y, CST_SCALE_DY);
}


void domino::resetSolver(){

    current_time    =   begin_t;
    y[0]            =   CST_INITIAL_TILT;
    y[1]            =   initial_angular_speed;
    gsl_odeiv2_driver_reset(gsl_driver);
}


void domino::resetDominoChain(){

    index = 0;
    begin_t = 0.;
    current_time = begin_t;
    elapsed_time = 0;
    tilt = CST_INITIAL_TILT;
    initial_angular_speed = CST_INITIAL_SPEED;
    angular_speed = initial_angular_speed;
    y[0] = tilt;
    y[1] = initial_angular_speed;
    gsl_odeiv2_driver_reset(gsl_driver);
    evalCollisionTime();
}


void domino::evalTilt(double t){

    assert(t >= current_time);
    int status = gsl_odeiv2_driver_apply(gsl_driver, &current_time, t, y);

    if (status != GSL_SUCCESS)
        cerr << "GSL domino solver failed. Error code : " << status << endl;

    assert(status == GSL_SUCCESS);
    double error        =       1.1 * CST_ABS_PRECISION + CST_REL_PRECISION*CST_SCALE_Y*tilt;
    tilt                =       y[0];
    angular_speed       =       y[1];
    angle_error         =       error;
    angular_speed_error =       error;
}


void domino::iterate(){

    assert (index < N_domino); // Stops right there if you try to iterate on a non-existing domino.

    // Make sure the solver can solve the equation at the collision time.
    resetSolver();

    // Bring the solver's state at the collision time
    evalTilt(collision_time);

    // Iterate the index value by one, set the initial conditions for the next domino.
    index++;
    begin_t                 =   0.;
    initial_angular_speed   =   1.*angular_speed;
    tilt                    =   CST_INITIAL_TILT;
    y[0]                    =   tilt;
    y[1]                    =   initial_angular_speed;

    // Evaluate the collision time for the new domino
    evalCollisionTime();
}


void domino::evalCollisionAngle(){

    const gsl_root_fsolver_type * T =   gsl_root_fsolver_bisection;
    gsl_root_fsolver * s            =   gsl_root_fsolver_alloc(T);
    gsl_function F;

    F.function  =   &collision_function;
    F.params    =   NULL;

    //Initializing parameters
    int status;
    int iter = 0, max_iter = 500;
    double xmin = 0.0, xmax = M_PI/2.0, x;
    double abserror = CST_ABS_PRECISION;
    double relerror = CST_REL_PRECISION;

    gsl_root_fsolver_set(s, &F, xmin, xmax);

    do{
        iter++;
        gsl_root_fsolver_iterate(s);
        xmin = gsl_root_fsolver_x_lower(s);
        xmax = gsl_root_fsolver_x_upper(s);
        x = gsl_root_fsolver_root(s);
        status = gsl_root_test_interval(xmin, xmax, abserror, relerror);
        if (status == GSL_SUCCESS ){
            collision_angle = x;
            collision_angle_error = abserror + relerror*min(abs(xmin), abs(xmax));
            gsl_root_fsolver_free(s);
        }
    } while (status == GSL_CONTINUE && iter < max_iter);

    assert(status == GSL_SUCCESS);
}


void domino::evalCollisionTime(){

    double prec = CST_ABS_PRECISION, midpoint, fmid;
    double time_step = 0.1;
    double a = begin_t, b = a;
    double y1 = evalTimeFunc(a), y2 = evalTimeFunc(b);
    int iter = 0, max_iter = 500;

    // Finding a compatible upper limit to perform bisection
    while (y1 * y2 > 0){
        b += time_step;
        y2 = evalTimeFunc(b);
    }

    assert ((a < b) && (y1*y2 < 0)); // By this point, a and b should be good. Exit if not.

    // Finding solution within prec

    midpoint = (b+a)/2;
    while (abs(b-a) >= prec && iter <= max_iter){
        //Debugging
        //cout << setprecision(10) << "a : " << a << endl;
        //cout << setprecision(10) << "b : " << b << endl;
        //cout << setprecision(10) << "eval(a) : " << y1 << endl;
        //cout << setprecision(10) << "eval(b) : " << y2 << endl;
        //cout << setprecision(10) << "Time : " << midpoint << endl;
        //cout << setprecision(10) << "Tilt : " << tilt << endl;
        //cout << setprecision(10) << "function : "  << tilt - collision_angle << endl << endl;

        iter++;
        midpoint = (a+b)/2;
        fmid = evalTimeFunc(midpoint);
        if (fmid == 0){
            collision_time = midpoint;
            break;
        }
        y1 = evalTimeFunc(a), y2 = evalTimeFunc(midpoint);
        if (y1 * y2 < 0){
            b = midpoint;
            y2 = fmid;
        }
        else{
            a = midpoint;
            y1 = fmid;
        }
    }

    collision_time = midpoint;
    elapsed_time += collision_time;
    // Resetting solver's initial conditions for the current domino for the rest of the execution.
    resetSolver();
  }


void domino::solveTiltOverTime(){

    int N_steps     =   1000;
    double t0       =   begin_t;
    double tl       =   collision_time;
    double h        =   (tl - t0)/N_steps;
    double ti;

    resetSolver();
    string filename =   "output/tilt_domino_";
    filename        +=  to_string(index);
    filename        +=  ".dat";
    fstream file;
    file.open(filename, ios::out);
    if (file.is_open()){
        for (int i=0 ; i <= N_steps ; i++){
            ti = t0 + i*h;
            evalTilt(ti);
            file << fixed << setprecision(10) << ti                     << "   "
                          << setprecision(10) << tilt                   << "   "
                          << setprecision(10) << angle_error            << "   "
                          << setprecision(10) << angular_speed          << "   "
                          << setprecision(10) << angular_speed_error    << endl;
        }
    }
    file.close();
}


void domino::solveWaveSpeed(){

    resetDominoChain();
    double error = CST_ABS_PRECISION;
    double velocity, velocity_err;

    string filename = "output/wave_speed.dat";
    fstream file;
    file.open(filename, ios::out);
    if (file.is_open()){
        while (index < N_domino){
            velocity        =   CST_DIST/collision_time;
            velocity_err    =   velocity * (error/collision_time);
            file << fixed << setw(2) << index               << "   "
                 << setprecision(10) << velocity            << "   "
                 << setprecision(10) << velocity_err        << "   "
                 << setprecision(10) << collision_time      << "   "
                 << setprecision(10) << error               << "   "
                 << setprecision(10) << elapsed_time        << endl;
            iterate();
        }
    }
    file.close();
}


double domino::evalTimeFunc(double x){
    // Because a bisection method doesn't evaluate the domino's tilt in a consistent stepping direction
    // it is required to reset the solver's parameters for the current domino.
    resetSolver();
    evalTilt(x);
    return tilt - collision_angle;
}
